#include "optimization_refactor.h"
#include <iostream>
#include <fstream>
#include <cstdlib>

// using namespace PathOptimization;

int main(int argc, char* argv[]) {
    if (argc < 2) return 1;
    std::ifstream ifs_path(argv[1]);
    Path<FrameType<1>> path;
    path.readFrames(ifs_path);
    if (argc == 3) {
        path.setFullLengthShift(atof(argv[2]));
    }
    double E = path.totalEnergy();
    std::cout << "Number of frames: " << path.numFrames() << std::endl;
    std::cout << "CV per frames: " << path.getFrame(0).size() << std::endl;
    std::cout << "Energy = " << E << std::endl;
//     path.dumpInfo(std::cout);
    FrameType<1> g0, g1, g2, g_last;
    path.gradient(0, g0);
    path.gradient(1, g1);
    path.gradient(2, g2);
    path.gradient(path.numFrames() - 1, g_last);
    for (size_t i = 0; i < path.numFrames(); ++i) {
        path.checkGradient(i, 0.05, 1e-5);
    }
    std::ofstream ofs_old("path_info_old.txt"), ofs_new("path_info_new.txt");
    path.dumpInfo(ofs_old);
    path.gradientDescent(1e-5, 500);
    path.dumpInfo(ofs_new);
    return 0;
}
