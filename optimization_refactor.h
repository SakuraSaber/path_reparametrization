#include <vector>
#include <initializer_list>
#include <numeric>
#include <iostream>
#include <array>
#include <iterator>
#include <string>
#include <cmath>
#include <algorithm>
#include <iomanip>

void splitString(const std::string& data, const std::string& delim, std::vector<std::string>& dest) {
    if (delim.empty()) {
        dest.push_back(data);
        return;
    }
    size_t index = 0, new_index = 0;
    std::string tmpstr;
    while (index != data.length()) {
        new_index = data.find(delim, index);
        if (new_index != std::string::npos) tmpstr = data.substr(index, new_index - index);
        else tmpstr = data.substr(index, data.length());
        if (!tmpstr.empty()) {
            dest.push_back(tmpstr);
        }
        if (new_index == std::string::npos) break;
        index = new_index + 1;
    }
}

template <typename T, int num_elem>
class CVType {
public:
    CVType(): m_data() {};
    CVType(std::initializer_list<T> list): m_data(list) {};
    size_t size() const {return m_data.size();}
    T& operator[](size_t i) {
        return m_data[i];
    }
    const T& operator[](size_t i) const {
        return m_data[i];
    }
    CVType& operator+=(const CVType& rhs) {
        for (size_t i = 0; i < size(); ++i) m_data[i] += rhs.m_data[i];
        return (*this);
    }
    CVType& operator-=(const CVType& rhs) {
        for (size_t i = 0; i < size(); ++i) m_data[i] -= rhs.m_data[i];
        return (*this);
    }
    CVType& operator*=(const T& rhs) {
        for (size_t i = 0; i < size(); ++i) m_data[i] *= rhs;
        return (*this);
    }
    CVType& operator/=(const T& rhs) {
        for (size_t i = 0; i < size(); ++i) m_data[i] /= rhs;
        return (*this);
    }
    auto begin() {
        return m_data.begin();
    }
    auto begin() const {
        return m_data.begin();
    }
    auto end() {
        return m_data.end();
    }
    auto end() const {
        return m_data.end();
    }
    const static size_t m_num_elem = num_elem;
    typedef T value_type;
private:
    std::array<T, num_elem> m_data;
};

template <typename T, int num_elem>
T distance(const CVType<T, num_elem>& A, const CVType<T, num_elem>& B) {
    T res = T();
    for (size_t i = 0; i < A.size(); ++i) {
        res += (A[i] - B[i]) * (A[i] - B[i]);
    }
    return std::sqrt(res);
}

template <typename T, int num_elem>
CVType<T, num_elem> operator+(CVType<T, num_elem> lhs, const CVType<T, num_elem>& rhs) {
    lhs += rhs;
    return lhs;
}

template <typename T, int num_elem>
CVType<T, num_elem> operator-(CVType<T, num_elem> lhs, const CVType<T, num_elem>& rhs) {
    lhs -= rhs;
    return lhs;
}

template <typename T, int num_elem>
CVType<T, num_elem> operator*(CVType<T, num_elem> lhs, const T& factor) {
    lhs *= factor;
    return lhs;
}

template <typename T, int num_elem>
CVType<T, num_elem> operator*(const T& factor, CVType<T, num_elem> rhs) {
    rhs *= factor;
    return rhs;
}

template <typename T, int num_elem>
T operator*(const CVType<T, num_elem>& lhs, const CVType<T, num_elem>& rhs) {
    return std::inner_product(lhs.begin(), lhs.end(), rhs.begin(), T());
}

template <typename T>
auto distance(const std::vector<T>& A, const std::vector<T>& B) {
    typename T::value_type x;
    x = decltype(x)();
    for (size_t i = 0; i < A.size(); ++i) {
        x += (A[i] - B[i]) * (A[i] - B[i]);
    }
    return std::sqrt(x);
}

template <typename T, int num_elem>
std::ostream& operator<<(std::ostream& os, const CVType<T, num_elem>& A) {
    std::ios_base::fmtflags f(os.flags());
    os << std::setprecision(5);
    os << std::fixed;
    if (num_elem > 1) os << "( ";
    for (const auto & i: A) {
        os << std::setw(12) << i << " ";
    }
    if (num_elem > 1) os << ")";
    os.flags(f);
    return os;
}

template <int num_elem> using FrameDataType = CVType<double, num_elem>;
template <int num_elem> using FrameType = std::vector<FrameDataType<num_elem>>;
template <int num_elem> using FrameGradientType = std::vector<FrameDataType<num_elem>>;

template <int num_elem>
FrameType<num_elem>& operator-=(FrameType<num_elem>& lhs, const FrameType<num_elem>& rhs) {
    std::transform(lhs.begin(), lhs.end(), rhs.begin(), lhs.begin(), std::minus<typename FrameType<num_elem>::value_type>());
    return lhs;
}

template <int num_elem>
FrameType<num_elem>& operator+=(FrameType<num_elem>& lhs, const FrameType<num_elem>& rhs) {
    std::transform(lhs.begin(), lhs.end(), rhs.begin(), lhs.begin(), std::plus<typename FrameType<num_elem>::value_type>());
    return lhs;
}

template <int num_elem>
FrameType<num_elem> operator-(FrameType<num_elem> lhs, const FrameType<num_elem>& rhs) {
    lhs -= rhs;
    return lhs;
}

template <int num_elem>
FrameType<num_elem> operator+(FrameType<num_elem> lhs, const FrameType<num_elem>& rhs) {
    lhs += rhs;
    return lhs;
}

template <int num_elem>
FrameType<num_elem>& operator*=(FrameType<num_elem>& lhs, const double& factor) {
    std::transform(lhs.begin(), lhs.end(), lhs.begin(), [&factor](auto x){return x * factor;});
    return lhs;
}

template <int num_elem>
FrameType<num_elem> operator*(FrameType<num_elem> lhs, const double& factor) {
    lhs *= factor;
    return lhs;
}

template <int num_elem>
FrameType<num_elem> operator*(const double& factor, FrameType<num_elem> rhs) {
    rhs *= factor;
    return rhs;
}

template <int num_elem>
double operator*(const FrameType<num_elem>& lhs, const FrameType<num_elem>& rhs) {
    return std::inner_product(lhs.begin(), lhs.end(), rhs.begin(), double(0.0));
}

template <int num_elem>
double rmse(const FrameType<num_elem>& lhs, const FrameType<num_elem>& rhs) {
    FrameType<num_elem> residue = lhs - rhs;
    const double rmse = std::sqrt((residue * residue) / static_cast<double>(residue.size()));
    return rmse;
}

template <int num_elem>
std::ostream& operator<<(std::ostream& os, const FrameType<num_elem>& A) {
    os << "{ ";
    for (const auto& item : A) {
        os << item << " "; 
    }
    os << "}";
    return os;
}

template <typename T>
double dot(const std::vector<T>& v1, const std::vector<T>& v2) {
    double result = 0.0;
    for (size_t i = 0; i < v1.size(); ++i) {
        result += v1[i] * v2[i];
    }
    return result;
}

template <typename T>
class Path {
public:
    Path();
    Path(const std::vector<T>& frames, double k_full_length = 1.0, double full_length_shift = 0.0);
    Path(const std::vector<T>& frames, const std::vector<double>& constants, double k_full_length = 1.0, double full_length_shift = 0.0);
    size_t numFrames() const {
        return m_frames.size();
    }
    size_t numSegments() const {
        return m_segment_length.size();
    }
    double totalEnergy() const;
    double fullLength() const;
    void gradient(size_t index, T& grad) const;
    void numericalGradient(size_t, T& grad, const double increment = 0.0001) const;
    T getFrame(size_t index) const;
    T setFrame(size_t index, const T& new_frame);
    void readFrames(std::istream& input_stream);
    std::ostream& dumpInfo(std::ostream& os) const;
    void checkGradient(size_t index, double increment_start, double increment_end) const;
    void gradientDescent(double initial_step_size, size_t max_iterations = 500, double min_rmse = 0.001);
    void setFullLengthShift(double full_length_shift) {m_full_length_shift = full_length_shift;}
    double computeRMSE(const Path& p2) const;
private:
    void updateSegment();
    std::vector<T> m_frames;
    std::vector<double> m_segment_length;
    std::vector<double> m_spring_constant;
    double m_k_full_length;
    double m_full_length_shift;
};

template <typename T>
Path<T>::Path() {}

template <typename T>
Path<T>::Path(const std::vector<T>& frames, double k_full_length, double full_length_shift):
m_frames(frames), m_segment_length(frames.size() - 1), m_spring_constant(m_segment_length.size()), m_k_full_length(k_full_length), m_full_length_shift(full_length_shift) {
    updateSegment();
    for (size_t i = 0; i < m_segment_length.size(); ++i) {
        m_spring_constant[i] = 1.0;
    }
}

template <typename T>
Path<T>::Path(const std::vector<T>& frames, const std::vector<double>& constants, double k_full_length, double full_length_shift):
m_frames(frames), m_segment_length(frames.size() - 1), m_spring_constant(m_segment_length.size()), m_k_full_length(k_full_length), m_full_length_shift(full_length_shift) {
    updateSegment();
    for (size_t i = 0; i < m_segment_length.size(); ++i) {
        m_spring_constant[i] = constants[i];
    }
}

template <typename T>
double Path<T>::totalEnergy() const {
    double total_energy = 0;
    for (size_t i = 0; i < numSegments() - 1; ++i) {
        total_energy += 0.5 * m_spring_constant[i] * (m_segment_length[i] - m_segment_length[i+1]) * (m_segment_length[i] - m_segment_length[i+1]);
    }
    // also restrain the l0 and l_last
    total_energy += 0.5 * m_spring_constant[numSegments() - 1] * (m_segment_length[0] - m_segment_length[numSegments() - 1]) * (m_segment_length[0] - m_segment_length[numSegments() - 1]);
    double full_length = fullLength();
    total_energy += 0.5 * m_k_full_length * (full_length - m_full_length_shift) * (full_length - m_full_length_shift);
    return total_energy;
}

template <typename T>
double Path<T>::fullLength() const {
    double full_length = 0;
    for (size_t i = 0; i < numSegments(); ++i) {
        full_length += m_segment_length[i];
    }
    return full_length;
}

template <typename T>
void Path<T>::gradient(size_t index, T& grad) const {
    // segment(n) consists of point(n) and point(n+1)
    if (index == 0) {
        // take derivatives with respect to segment(0)
        // ∂l(0) / ∂point(0) = 1/l(0) * (point(0) - point(1))
        auto dli_dpi = 1.0 / m_segment_length[index] * (m_frames[index] - m_frames[index + 1]);
        // ∂E / ∂point(0) = ∂E(0) / ∂point(0) + ∂E(first_last) / ∂point(0) + ∂E(full) / ∂point(0)
        grad = dli_dpi * m_spring_constant[index] * (m_segment_length[index] - m_segment_length[index + 1]) + dli_dpi * m_spring_constant[numSegments() - 1] * (m_segment_length[index] - m_segment_length[numSegments() - 1]) + dli_dpi * m_k_full_length * (fullLength() - m_full_length_shift);
    } else if (index == m_frames.size() - 1) {
        auto dli_dpi = -1.0 / m_segment_length[index - 1] * (m_frames[index - 1] - m_frames[index]);
        grad = -1.0 * dli_dpi * m_spring_constant[index - 2] * (m_segment_length[index - 2] - m_segment_length[index - 1]) + -1.0 * dli_dpi * m_spring_constant[index - 1] * (m_segment_length[0] - m_segment_length[index - 1]) + dli_dpi * m_k_full_length * (fullLength() - m_full_length_shift);
    } else if (index > 1 && index < m_frames.size() - 2) {
        // ∂l(i) / ∂point(i)
        auto dli_dpi = 1.0 / m_segment_length[index] * (m_frames[index] - m_frames[index + 1]);
        // ∂l(i-1) / ∂point(i)
        auto dli_dpi_prev = -1.0 / m_segment_length[index - 1] * (m_frames[index - 1] - m_frames[index]);
        auto dEi_dpi_next = m_spring_constant[index] * (m_segment_length[index] - m_segment_length[index + 1]) * dli_dpi;
        auto dEi_dpi = m_spring_constant[index - 1] * (m_segment_length[index - 1] - m_segment_length[index]) * (dli_dpi_prev - dli_dpi);
        auto dEi_dpi_prev = -1.0 * dli_dpi_prev * m_spring_constant[index - 2] * (m_segment_length[index - 2] - m_segment_length[index - 1]);
        // ∂E_full / ∂point(i) = k_full * (l_full - L0) * (∂l(i) / ∂point(i) + ∂l(i-1) / ∂point(i))
        auto dEfull_dpi = m_k_full_length * (fullLength() - m_full_length_shift) * (dli_dpi + dli_dpi_prev);
        // ∂E / ∂point(i) = ∂E(i) / ∂point(i) + ∂E(i-1) / ∂point(i) + ∂E_full / ∂point(i)
        grad = dEi_dpi_next + dEi_dpi + dEi_dpi_prev + dEfull_dpi;
    } else if (index == 1) {
        auto dli_dpi_prev = -1.0 / m_segment_length[index - 1] * (m_frames[index - 1] - m_frames[index]);
        auto dli_dpi = 1.0 / m_segment_length[index] * (m_frames[index] - m_frames[index + 1]);
        auto dEi_dpi_prev = m_spring_constant[index - 1] * (m_segment_length[index - 1] - m_segment_length[index]) * (dli_dpi_prev - dli_dpi);
        auto dEi_dpi = m_spring_constant[index] * (m_segment_length[index] - m_segment_length[index + 1]) * dli_dpi;
        auto dEfull_dpi = m_k_full_length * (fullLength() - m_full_length_shift) * (dli_dpi + dli_dpi_prev);
        grad = dEi_dpi + dEi_dpi_prev + dEfull_dpi + dli_dpi_prev * m_spring_constant[numSegments() - 1] * (m_segment_length[0] - m_segment_length[numSegments() - 1]);
    } else if (index == m_frames.size() - 2) {
        auto dli_dpi_prev = -1.0 / m_segment_length[index - 1] * (m_frames[index - 1] - m_frames[index]);
        auto dli_dpi = 1.0 / m_segment_length[index] * (m_frames[index] - m_frames[index + 1]);
        auto dEi_dpi_prev = m_spring_constant[index - 1] * (m_segment_length[index - 1] - m_segment_length[index]) * (dli_dpi_prev - dli_dpi);
        auto dEi_dpi_prev_prev = -1.0 * m_spring_constant[index - 2] * (m_segment_length[index - 2] - m_segment_length[index - 1]) * dli_dpi_prev;
        auto dEfull_dpi = m_k_full_length * (fullLength() - m_full_length_shift) * (dli_dpi + dli_dpi_prev);
        grad = dEi_dpi_prev_prev + dEi_dpi_prev + dEfull_dpi + -1.0 * dli_dpi * m_spring_constant[numSegments() - 1] * (m_segment_length[0] - m_segment_length[numSegments() - 1]);
    }
    else {
        std::cerr << "index out of boundary!\n";
        return;
    }
}

template <typename T>
void Path<T>::numericalGradient(size_t index, T& grad, const double increment) const {
    grad.resize(this->m_frames[0].size());
    const size_t element_size = m_frames[0][0].size();
    for (size_t i = 0; i < m_frames[index].size(); ++i) {
        for (size_t j = 0; j < element_size; ++j) {
            Path p_next(*this);
            Path p_prev(*this);
            p_next.m_frames[index][i][j] += increment;
            p_next.updateSegment();
            p_prev.m_frames[index][i][j] -= increment;
            p_prev.updateSegment();
            grad[i][j] = (p_next.totalEnergy() - p_prev.totalEnergy()) / (2.0 * increment);
        }
    }
}

template <typename T>
T Path<T>::getFrame(size_t index) const {
    return m_frames[index];
}

template <typename T>
T Path<T>::setFrame(size_t index, const T& new_frame) {
    T old = m_frames[index];
    m_frames[index] = new_frame;
    updateSegment();
    return old;
}

template <typename T>
void Path<T>::updateSegment() {
    if (m_segment_length.size() != m_frames.size() - 1) m_segment_length.resize(m_frames.size() - 1);
    for (size_t i = 0; i < m_frames.size() - 1; ++i) {
        m_segment_length[i] = distance(m_frames[i], m_frames[i+1]);
    }
}

template <typename T>
void Path<T>::readFrames(std::istream& input_stream) {
    std::string line;
    std::vector<T> new_frame;
    const size_t num_elem = T::value_type::m_num_elem;
    while (std::getline(input_stream, line)) {
        std::vector<std::string> fields;
        splitString(line, " ", fields);
        if (fields.size() > 0 && (fields.size() % num_elem  == 0)) {
            T p(static_cast<size_t>(fields.size() / num_elem));
            for (size_t i = 0; i < p.size(); ++i) {
                for (size_t j = 0; j < num_elem; ++j) {
                    p[i][j] = std::stod(fields[i*num_elem+j]);
                }
            }
            new_frame.push_back(p);
        }
    }
    m_frames = new_frame;
    updateSegment();
    m_spring_constant.resize(m_segment_length.size());
    for (size_t i = 0; i < m_segment_length.size(); ++i) {
        m_spring_constant[i] = 1.0;
    }
    m_k_full_length = 1.0;
    m_full_length_shift = fullLength();
}

template <typename T>
std::ostream& Path<T>::dumpInfo(std::ostream& os) const {
    os << "# Points \n";
    for (size_t i = 0; i < numFrames(); ++i) {
        os << m_frames[i] << std::endl;
    }
    os << "# Segment lengths \n";
    for (size_t i = 0; i < numSegments(); ++i) {
        os << m_segment_length[i] << std::endl;
    }
    os << "# Full length: " << fullLength() << std::endl;
    return os;
}

template <typename T>
void Path<T>::checkGradient(size_t index, double increment_start, double increment_end) const {
    if (increment_start < 0 || increment_end < 0) return;
    if (increment_start <= increment_end) return;
    T grad, num_grad;
    double increment = increment_start;
    this->gradient(index, grad);
    std::printf("Check gradient of index %lu\n", index);
    while (increment > increment_end) {
        this->numericalGradient(index, num_grad, increment);
        std::printf("dx = %12.9lf ; rmse = %12.9lf\n", increment, rmse(grad, num_grad));
        increment /= 5.0;
    }
}

template <typename T>
void Path<T>::gradientDescent(double initial_step_size, size_t max_iterations, double min_rmse) {
    if (initial_step_size < 0) return;
    for (size_t iter = 0; iter < max_iterations; ++iter) {
        T current_point, next_point;
        double step_size;
        double current_energy = 0, next_energy = 0;
        T current_grad, next_grad;
        const Path<T> old_path(*this);
        for (size_t i = 0; i < numFrames(); ++i) {
            step_size = initial_step_size;
            current_energy = next_energy = 0;
            size_t loop_count = 0;
            do {
                if (step_size < 0 || std::isnan(step_size)) {
                    current_energy = next_energy = 0;
                    continue;
                }
                // save current point
                current_point = m_frames[i];
                // current function value
                current_energy = totalEnergy();
                // current gradient
                T current_grad, next_grad;
                gradient(i, current_grad);
                // move point
                m_frames[i] = m_frames[i] - step_size * current_grad;
                // save point of next step
                next_point = m_frames[i];
                // compute energy again
                updateSegment();
                next_energy = totalEnergy();
                // gradient of next step
                gradient(i, next_grad);
                // update step size
                step_size = dot(current_point - next_point, current_grad - next_grad) / dot(current_grad - next_grad, current_grad - next_grad);
                ++loop_count;
//                 printf("Iteration[%lu], point[%lu], loop = %lu, stepsize = %12.9lf, energy = %12.9lf, energy_next = %12.9lf\n", iter, i, loop_count, step_size, current_energy, next_energy);
            }
            while (current_energy > next_energy);
            // restore the point since it's changed at the last step
            m_frames[i] = current_point;
            updateSegment();
        }
        const double rmse = computeRMSE(old_path);
        printf("Iteration[%lu] finished, RMSE = %12.9lf\n", iter, rmse);
        if (rmse <= min_rmse) break;
        if (iter == max_iterations - 1 && rmse > min_rmse) {
            printf("Warning! the number of iteration[%lu] reaches the maximum, but RMSE = %lf is still larger than threshold %lf.\n", iter, rmse, min_rmse);
        }
    }
}

template <typename T>
double Path<T>::computeRMSE(const Path& p2) const {
    double rmsd = 0.0;
    for (size_t i = 0; i < numFrames(); ++i) {
        const auto diff = m_frames[i] - p2.m_frames[i];
        rmsd += diff * diff;
    }
    return std::sqrt(rmsd / static_cast<double>(numFrames()));
}
