#!/usr/bin/env python3
import numpy as np

def func(x):
    return 8.0 * np.sin(0.5 * x)

points_x = np.arange(-10, 10, 0.5)
rands_x = 0.5 * np.random.rand(np.size(points_x))
points_x = points_x + rands_x
points_y = func(points_x)

np.savetxt('test_points.txt', np.c_[points_x, points_y], fmt = '%12.6f')
